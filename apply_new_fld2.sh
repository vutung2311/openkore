#!/bin/sh

perl fields/tools/gat_to_fld2.pl

if [ $# -eq 0 ]; then
find . -maxdepth 1 -name "*.fld2" -exec cp {} fields/ \;
else
find . -maxdepth 1 -name "*.fld2" -exec cp {} fields/$1/ \;
fi

rm -f fields/*.dist fields/*.weight *.fld2 *.rsw *.gat && truncate -s0 tables/portalsLOS.txt