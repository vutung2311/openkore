#------------------------------------------------------------
# Geographer plugin - OpenKore Team
#
# Prohibit attacking a geographer:
# + healed by another one
# + situated too close to another one
# Stop attacking monsters under above circumstances
# (prohibit to attack means setting up null damage from kore
# and 1 damage from player (from kore's id))
#------------------------------------------------------------

package geographer;

use strict;
use Globals;
use Log;
use Misc;
use Plugins;
use Utils;
use AI::Attack;
use Translation qw( T TF );
use Log qw(message warning error debug);

Plugins::register("geographer", "Reaction on two close ranged monsters", \&Unload);
my $hooks = Plugins::addHooks(
	["AI_pre",\&positionReact, undef],
	["packet_skilluse", \&healReact, undef]);

my $monstersNameList = $config{checkingTooClosedRangedMonsters} // "Drosera,Muscipular,Geographer,Nephentes";

sub positionReact {
	for my $monster1 (@$monstersList) {
		next if !Utils::existsInList($monstersNameList, $monster1->{name});
		for my $monster2 (@$monstersList) {
			next if !Utils::existsInList($monstersNameList, $monster2->{name});
			next if $monster1->{ID} eq $monster2->{ID};
			next if distance($monster1->{pos},$monster2->{pos}) > 10;
			next if ($monster1->{ignore} eq 1 && $monster2->{ignore} eq 1);

			my $monsterName1 = $monster1->{name};
			my $monsterID1 = $monster1->{binID};
			my $monsterName2 = $monster2->{name};
			my $monsterID2 = $monster2->{binID};

 			message TF("Stop attacking monsters %s (%s) and %s (%s) because they are too close\n", $monsterName1, $monsterID1, $monsterName2, $monsterID2), "ai_attack";
			prohibitAttacking($monster1);
			prohibitAttacking($monster2);
			useTeleport(1);
			last;
		}
	}
}

sub healReact {
	my (undef, $args) = @_;
	my $sourceID = $args->{sourceID};
	my $targetID = $args->{targetID};
	my $skillID = $args->{skillID};
	my $monsterS = $monsters{$sourceID};
	my $monsterO = $monsters{$targetID};
	if(!$monsterO
	  || $skillID != 28
	  || !Utils::existsInList($monstersNameList, $monsterO->{name})) {
		return;
	}
	prohibitAttacking($monsterO);
	if (Utils::existsInList($monstersNameList, $monsterS->{name})) {
		prohibitAttacking($monsterS);
	}
}

sub prohibitAttacking {
	@_[0]->{ignore} = 1;
	my $ID = @_[0]->{ID};
	my $target = Actor::get($ID);
	$target->{attack_failed} = time if ($monsters{$ID});
	AI::dequeue;
}

sub Unload {
	Plugins::delHooks($hooks);
}
1;