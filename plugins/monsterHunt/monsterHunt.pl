package monsterHunt;
 
use strict;
use Plugins;
use Settings;
use Commands;
use Globals qw(%config);
use Utils;
use Misc;
use Log qw(message error warning debug);
 
#########
# startup
Plugins::register('monsterHunt', 'Monster hunt quest fulfiller', \&Unload, \&Unload);

my $hooks = Plugins::addHooks(
	['monster_disappeared', \&onMonsterDisappeared, undef],
	['quest_mission_updated', \&onUpdate, undef],
	['pos_load_config.txt', \&clearMonsterHuntCount, undef]
);

my $monsterHuntedCount = ();
my $monsterHuntingFinished = ();
my $monstersToHunt = undef;
use constant MONSTER_HUNT_LOG_TAG => "monsterHunt";

# onUnload
sub Unload {
	Plugins::delHooks($hooks);
}

sub clearMonsterHuntCount() {
	foreach my $key (keys(%{$monsterHuntedCount})) {
		delete($monsterHuntedCount->{$key});
	}
	foreach my $key (keys(%{$monsterHuntingFinished})) {
		delete($monsterHuntingFinished->{$key});
	}
	undef($monstersToHunt);
}

sub onMonsterDisappeared {
	my (undef, $args) = @_;
	my $monster = $args->{monster};
	my $slot = 0;

	return unless ($monster->{dead} == 1 && ($monster->{dmgFromYou} > 0 || $monster->{dmgFromParty} > 0));

	while (exists $config{"monsterHuntSlot_${slot}"}) {
		my $monsterIDOrName = $config{"monsterHuntSlot_${slot}"};
		my $mustHuntAmount = $config{"monsterHuntSlot_${slot}_count"};
		my $commandList = $config{"monsterHuntSlot_${slot}_doCommands"};
		my $huntedAmount = $monsterHuntedCount->{$monsterIDOrName} ? $monsterHuntedCount->{$monsterIDOrName} : 0;
		debug "Then do commands ${commandList}\n", MONSTER_HUNT_LOG_TAG;
		message "Hunted " . $huntedAmount . "/" . $mustHuntAmount . " monster ${monsterIDOrName}\n", MONSTER_HUNT_LOG_TAG;
		
		goto NEXT if ($monster->{name} ne $monsterIDOrName && $monster->{nameID} ne $monsterIDOrName);
		$monsterHuntedCount->{$monsterIDOrName}++;

		goto NEXT if $monsterHuntedCount->{$monsterIDOrName} < $mustHuntAmount;
		debug "Finished hunting monster ${monsterIDOrName}\n", MONSTER_HUNT_LOG_TAG;
		$monsterHuntingFinished->{$monsterIDOrName} = 1;
		
		my @commands = split / *, */, $commandList;
		foreach(@commands) {
			debug "Running command " . $_ . "\n", MONSTER_HUNT_LOG_TAG;
			Commands::run($_);
		}
NEXT:
		$slot++;
	}
	if (!defined($monstersToHunt)) {
		$monstersToHunt = $slot;
	}

	my @commands = split / *, */, $config{"monsterHuntDone_doCommands"};
	if (scalar(keys(%{$monsterHuntingFinished})) eq $monstersToHunt) {
		foreach(@commands) {
			debug "Running command " . $_ . "\n", MONSTER_HUNT_LOG_TAG;
			Commands::run($_);
		}
	}
}

sub onUpdate {
	my (undef, $args) = @_;
	my $slot = 0;
	my $count = $args->{count};
	my $goal = $args->{goal};
	my $mobID = $args->{mobID};

	return if $count < $goal;

	while (exists $config{"monsterHuntSlot_${slot}"}) {
		my $monsterIDOrName = $config{"monsterHuntSlot_${slot}"};
		my $mustHuntAmount = $config{"monsterHuntSlot_${slot}_count"};
		my $commandList = $config{"monsterHuntSlot_${slot}_doCommands"};
		my $huntedAmount = $monsterHuntedCount->{$monsterIDOrName} ? $monsterHuntedCount->{$monsterIDOrName} : 0;
		debug "Then do commands ${commandList}\n", MONSTER_HUNT_LOG_TAG;
		message "Hunted " . $huntedAmount . "/" . $mustHuntAmount . " monster ${monsterIDOrName}\n", MONSTER_HUNT_LOG_TAG;
		
		goto NEXT if $mobID ne $monsterIDOrName;
		debug "Finished hunting monster ${monsterIDOrName}\n", MONSTER_HUNT_LOG_TAG;
		$monsterHuntingFinished->{$monsterIDOrName} = 1;
		
		my @commands = split / *, */, $commandList;
		foreach(@commands) {
			debug "Running command " . $_ . "\n", MONSTER_HUNT_LOG_TAG;
			Commands::run($_);
		}
NEXT:
		$slot++;
	}
	if (!defined($monstersToHunt)) {
		$monstersToHunt = $slot;
	}

	my @commands = split / *, */, $config{"monsterHuntDone_doCommands"};
	if (scalar(keys(%{$monsterHuntingFinished})) eq $monstersToHunt) {
		foreach(@commands) {
			debug "Running command " . $_ . "\n", MONSTER_HUNT_LOG_TAG;
			Commands::run($_);
		}
	}
}

return 1;
