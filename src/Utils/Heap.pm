package Utils::Heap;
use strict;
use warnings;

sub new {
    my $class   = shift;
    my $costSub = shift;
    if ( !defined($costSub) ) {
        $costSub = sub { return int( $_[0] ) };
    }
    my $self =
      { "_Heap" => [], "OrderOK" => 1, CurrentSize => 0, CostSub => $costSub };
    $self = bless( $self, $class );
    return $self;
}

sub FindMin {
    my $self = shift;
    $self->FixHeap() if $self->{OrderOK} == 0;
    return $self->{_Heap}->[1] || undef;    #returns an array ref.
}

sub Toss {
    my $self      = shift;
    my $node_aref = shift;

    # remember that idx=0 is unused. So can't call perl's push.
    my $idx = ++$self->{CurrentSize};
    $self->{_Heap}->[$idx] = $node_aref;

    my $parent_aref = $self->{_Heap}->[ int( $self->{CurrentSize} / 2 ) ];
    return unless ($parent_aref);    #may be the root, so no parent defined.
    my $parent_file_sz = $self->{CostSub}->($parent_aref);
    my $newfile_sz     = $self->{CostSub}->($node_aref);

    $self->{OrderOK} = 0
      if ( $newfile_sz < $parent_file_sz );    #flip this for a maximum heap
}

sub Insert {
    my $self       = shift;
    my $x          = shift;                    # percolate x up as needed
    my $newfile_sz = $self->{CostSub}->($x);

    # If Order is not OK, just toss it.
    if ( $self->{OrderOK} == 0 ) {
        $self->Toss($x);
        return;
    }

    # Create a new empty (Hole) position at the end of Heap
    my $Hole = ++$self->{CurrentSize};
    for (
        ;
        ( $Hole > 1 )
          && ( $newfile_sz <
            $self->{CostSub}->( $self->{_Heap}->[ int( $Hole / 2 ) ] ) ) ;
        $Hole = int( $Hole / 2 )
      )
    {
        $self->{_Heap}->[$Hole] = $self->{_Heap}->[ int( $Hole / 2 ) ];
    }
    $self->{_Heap}->[$Hole] = $x;
}

sub DeleteMin {
    my $self = shift;

    # remove the last entry in Heap and put it at root position
    $self->{_Heap}->[1] = $self->{_Heap}->[ $self->{CurrentSize} ];
    delete $self->{_Heap}->[ $self->{CurrentSize} ];
    --$self->{CurrentSize};
    $self->PercolateDown(1);    # fix heap order
}

sub PercolateDown {
    my $self     = shift;
    my $Hole     = shift;
    my $tmp_aref = $self->{_Heap}->[$Hole];
    my $tmp_sz =
      $self->{CostSub}->( $self->{_Heap}->[$Hole] )
      ;                         #this is what our comparison is based on

    my $Child;
    for ( ; ( $Hole * 2 ) <= $self->{CurrentSize} ; $Hole = $Child ) {
        $Child = $Hole * 2;     #get left Child
        my $Child_sz = $self->{CostSub}->( $self->{_Heap}->[$Child] );

 # if $Child is not the last element, check if the right child value is smaller.
        if (
            ( $Child != $self->{CurrentSize} )
            && ( $self->{CostSub}->( $self->{_Heap}->[ $Child + 1 ] ) <
                $Child_sz )
          )
        {
            $Child++;    # become right child since it exists and it is smaller.
        }
        if ( $self->{CostSub}->( $self->{_Heap}->[$Child] ) < $tmp_sz ) {
            $self->{_Heap}->[$Hole] = $self->{_Heap}->[$Child];
        }
        else {
            last;
        }
    }
    $self->{_Heap}->[$Hole] = $tmp_aref;
}

sub FixHeap {
    my $self = shift;
    for ( my $i = int( ( $self->{CurrentSize} ) / 2 ) ; $i > 0 ; $i-- ) {
        $self->PercolateDown($i);
    }
    $self->{OrderOK} = 1;
}

sub print_heap {
    my $self = shift;
    for ( my $i = 1 ; $i < scalar @{ $self->{_Heap} } ; $i++ ) {
        print "\t @{ $self->{_Heap}->[$i] } \n";
    }
}
1;
