package AITest;

use strict;
use warnings;

use Test::More;
use Test::Deep;
use AI;

sub start {
	subtest 'test AI::queue and AI::dequeue' => sub {
		AI::clear();
        AI::queue('teleport', {lvl => 1});
		is( AI::action(), 'teleport' );
		cmp_deeply( AI::args(), {lvl => 1} );
        AI::dequeue();
		cmp_deeply( @AI::ai_seq, () );
		cmp_deeply( @AI::ai_seq_args, () );
		done_testing();
	};
	subtest 'test AI::queue and AI::clear' => sub {
		AI::clear();
        AI::queue('teleport', {lvl => 1});
        AI::queue('route', {attackID => 1});
        AI::queue('items_take', {ai_items_take_delay => 0.4});
        AI::queue('skill_use', {tag => 'attackSkill'});
		is( AI::action(), 'skill_use' );
		cmp_deeply( AI::args(), {tag => 'attackSkill'} );
		cmp_deeply( \@AI::ai_seq, bag('skill_use', 'items_take', 'route', 'teleport') );
		cmp_deeply( \@AI::ai_seq_args, bag({tag => 'attackSkill'}, {ai_items_take_delay => 0.4}, {attackID => 1}, {lvl => 1}) );
		AI::clear('route', 'skill_use');
		cmp_deeply( \@AI::ai_seq, bag('items_take', 'teleport') );
		cmp_deeply( \@AI::ai_seq_args, bag({ai_items_take_delay => 0.4}, {lvl => 1}) );
		done_testing();
	};
	subtest "test AI::clear('teleport') and AI::queue('teleport') continuously" => sub {
		AI::clear();
        AI::queue('teleport', {lvl => 1});
        AI::queue('route', {attackID => 1});
        AI::queue('items_take', {ai_items_take_delay => 0.4});
        AI::queue('skill_use', {tag => 'attackSkill'});
		is( AI::action(), 'skill_use' );
		cmp_deeply( AI::args(), {tag => 'attackSkill'} );
		cmp_deeply( \@AI::ai_seq, bag('skill_use', 'items_take', 'route', 'teleport') );
		cmp_deeply( \@AI::ai_seq_args, bag({tag => 'attackSkill'}, {ai_items_take_delay => 0.4}, {attackID => 1}, {lvl => 1}) );
		AI::clear('teleport');
		AI::queue('teleport', {lvl => 1});
		AI::clear('teleport');
		AI::queue('teleport', {lvl => 1});
		AI::clear('teleport');
		AI::queue('teleport', {lvl => 1});
		cmp_deeply( \@AI::ai_seq, bag('teleport', 'skill_use', 'items_take', 'route') );
		cmp_deeply( \@AI::ai_seq_args, bag({lvl => 1}, {tag => 'attackSkill'}, {ai_items_take_delay => 0.4}, {attackID => 1}) );
		done_testing();
	};
	subtest "test AI::clearBefore('attack')" => sub {
		AI::clear();
		AI::queue('skill_use', {tag => 'attackSkill'});
        AI::queue('attack', {attackID => 1});
        AI::queue('route', {attackID => 1});
        AI::queue('move', {attackID => 1});
		is( AI::action(), 'move' );
		cmp_deeply( AI::args(), {attackID => 1} );
		cmp_deeply( \@AI::ai_seq, bag('move', 'route', 'attack', 'skill_use') );
		cmp_deeply( \@AI::ai_seq_args, bag({attackID => 1}, {attackID => 1}, {attackID => 1}, {tag => 'attackSkill'}) );
		AI::clearBefore("attack");
		is( AI::action(), 'attack' );
		cmp_deeply( AI::args(), {attackID => 1} );
		cmp_deeply( \@AI::ai_seq, bag('attack', 'skill_use') );
		cmp_deeply( \@AI::ai_seq_args, bag({attackID => 1}, {tag => 'attackSkill'}) );
		AI::queue('teleport', {lvl => 1});
		AI::queue('route', {attackID => 1});
		AI::queue('move', {attackID => 1});
		AI::clearBefore("attack", "route", "move");
		cmp_deeply( \@AI::ai_seq, bag('teleport', 'attack', 'skill_use') );
		cmp_deeply( \@AI::ai_seq_args, bag({lvl => 1}, {attackID => 1}, {tag => 'attackSkill'}) );
		done_testing();
	};
}

1;
