package Utils::HeapTest;

use strict;
use warnings;

use Test::More;
use Test::Deep;
use Utils::Heap;

sub start {
	subtest 'test min heap with no cost sub routine' => sub {
		my $heap = Utils::Heap->new();
		isnt($heap, undef);
		$heap->Insert(3);
		is($heap->FindMin(), 3);
		$heap->Insert(2);
		is($heap->FindMin(), 2);
		$heap->Insert(1);
		is($heap->FindMin(), 1);
		$heap->Insert(1);
		is($heap->FindMin(), 1);
		$heap->DeleteMin();
		is($heap->FindMin(), 1);
		$heap->DeleteMin();
		is($heap->FindMin(), 2);
		$heap->DeleteMin();
		is($heap->FindMin(), 3);
		$heap->DeleteMin();
		is($heap->FindMin(), undef);
		done_testing();
	};
	subtest 'test min heap with cost sub routine' => sub {
		my $heap = Utils::Heap->new(sub { return $_[0]->{deadline}; });
		isnt($heap, undef);
		$heap->Insert({val => 'three', deadline => 3});
		cmp_deeply($heap->FindMin(), {val => 'three', deadline => 3});
		$heap->Insert({val => 'two', deadline => 2});
		cmp_deeply($heap->FindMin(), {val => 'two', deadline => 2});
		$heap->Insert({val => 'one', deadline => 1});
		cmp_deeply($heap->FindMin(), {val => 'one', deadline => 1});
		done_testing();
	};
}

1;
