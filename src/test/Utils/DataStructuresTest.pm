package Utils::DataStructuresTest;

use strict;
use warnings;

use Test::More;
use Utils::DataStructures qw(binAdd binRemove);
use Test::More;
use Test::Deep;

sub start {
	subtest 'test binRemove' => sub {
		my @arr = (1, 2, 3, 4);
        binRemove(\@arr, 3);
        cmp_deeply(\@arr, bag(1, 2, 4));
		done_testing();
	};
	subtest 'test binAdd #1' => sub {
		my @arr = (1, 2, 3, 4);
        binAdd(\@arr, 5);
        cmp_deeply(\@arr, bag(1, 2, 3, 4, 5));
		done_testing();
	};
	subtest 'test binAdd #2' => sub {
		my @arr = (1, undef, 3, 4);
        binAdd(\@arr, 5);
        cmp_deeply(\@arr, bag(1, 5, 3, 4));
		done_testing();
	};
}

1;
