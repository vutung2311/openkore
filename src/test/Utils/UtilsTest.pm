package Utils::UtilsTest;

use strict;
use warnings;

use Test::More;
use Utils qw(timeOut);
use Time::HiRes qw ( time );

sub start {
	subtest 'test timeOut' => sub {
		is( timeOut({time => time() - 10, timeout => 11}), 0 );
		is( timeOut({time => time() - 11, timeout => 10}), 1 );
		is( timeOut(time() - 10, 11), 0 );
		is( timeOut(time() - 11, 10), 1 );
		is( timeOut(time() - 1, 1.1), 0 );
		is( timeOut(time() - 1.1, 1), 1 );
		is( timeOut(time() - 0.00001, 0.000001), 1 );
		is( timeOut(time() - 0.000001, 0.0001), 0 );
		done_testing();
	};
}

1;
