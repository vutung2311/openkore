#########################################################################
#  OpenKore - X-Kore
#
#  This software is open source, licensed under the GNU General Public
#  License, version 2.
#  Basically, this means that you're allowed to modify and distribute
#  this software. However, if you distribute modified versions, you MUST
#  also distribute the source code.
#  See http://www.gnu.org/licenses/gpl.html for the full license.
#
#  $Revision$
#  $Id$
#
#########################################################################
# Note: the difference between XKore2 and XKoreProxy is that XKore2 can
# work headless (it handles all server messages by itself), while
# XKoreProxy lets the RO client handle many server messages.
package Network::XKoreProxy;

use strict;
use base qw(Exporter);
use Exporter;
use IO::Socket::IP;
use Time::HiRes qw(time usleep);
use utf8;
use Win32::API;
my $DivertPort = Win32::API::More->new(
    'RagnarokDivert', 'int DivertPort(UINT16 proxyThreadIdx, UINT16 remotePort, UINT16 proxyPort, UINT16 alternativePort)'
);
die "Error: Failed to load DivertPort: ".(Win32::FormatMessage(Win32::GetLastError())) unless $DivertPort;

use Modules 'register';
use Globals;
use Globals qw(%config $masterServer);
use Log qw(message warning error debug);
use Utils qw(dataWaiting timeOut makeIP encodeIP swrite existsInList);
use Misc qw(configModify visualDump);
use Translation qw(T TF);
use I18N qw(bytesToString);
use Interface;

use Network;
use Network::Send ();
use Utils::Exceptions;
use Network::MessageTokenizer;

my $clientBuffer;
my %flushTimer;
my $currentClientKey = 0;

# Members:
#
# Socket proxy_listen
#    A server socket which accepts new connections from the RO client.
#    This is only defined when the RO client hasn't already connected
#    to XKoreProxy.
#
# Socket proxy
#    A client socket, which connects XKoreProxy with the RO client.
#    This is only defined when the RO client has connected to XKoreProxy.

##
# Network::XKoreProxy->new()
#
# Initialize X-Kore-Proxy mode.
sub new {
	my $class = shift;
	my $self = bless {}, $class;

	# Reuse code from Network::DirectConnection to connect to the server
	require Network::DirectConnection;
	$self->{server} = new Network::DirectConnection($self);

	$incomingMessages = new Network::MessageTokenizer(\%rpackets);
	$self->{publicIP} = $config{XKore_publicIp} || undef;
	$self->{client_state} = 0;
	$self->{proxy_listens} = {};
	$self->{nextIp} = undef;
	$self->{nextPort} = undef;
	$self->{proxyListenIp} = undef;
	$self->{proxyPort} = undef;
	$self->{divertedPorts} = ();
	$self->{divertIndex} = 0;
	$self->{charServerIp} = undef;
	$self->{charServerPort} = undef;
	$self->{gotError} = 0;
	{
		no encoding 'utf8';
		$self->{packetPending} = '';
		$clientBuffer = '';
	}

	message T("X-Kore mode intialized.\n"), "startup";

	return $self;
}

sub version {
	return 1;
}

sub DESTROY {
	my $self = shift;

	for my $proxy_listen (values(%{$self->{proxy_listens}})) {
		close($proxy_listen);
	};
	close($self->{proxy});
}


######################
## Server Functions ##
######################

sub serverAlive {
	my $self = shift;
	return $self->{server}->serverAlive;
}

sub serverConnect {
	my $self = shift;
	my $host = shift;
	my $port = shift;

	return $self->{server}->serverConnect($host, $port);
}

sub serverPeerHost {
	my $self = shift;
	return $self->{server}->serverPeerHost if ($self->serverAlive);
	return undef;
}

sub serverPeerPort {
	my $self = shift;
	return $self->{server}->serverPeerPort if ($self->serverAlive);
	return undef;
}

sub serverRecv {
	my $self = shift;
	return $self->{server}->serverRecv();
}

sub serverSend {
	my $self = shift;
	my $msg = shift;

	$self->{server}->serverSend($msg);
}

sub serverDisconnect {
	my $self = shift;
	my $preserveClient = shift;

	return unless (defined($self->{server}));

	close($self->{proxy}) if (defined($self->{proxy}) && !$preserveClient);
	if (!$preserveClient) {
		$self->{nextIp} = undef;
		$self->{nextPort} = undef;
	}
	$self->{waitClientDC} = 1 if $preserveClient;

	# user has played with relog command.
	if ($timeout_ex{'master'}{'time'}) {
		undef $timeout_ex{'master'}{'time'};
	}
	return $self->{server}->serverDisconnect();
}

sub serverAddress {
	my ($self) = @_;
	return $self->{server}->serverAddress();
}

sub getState {
	my ($self) = @_;
	return $self->{server}->getState();
}

sub setState {
	my ($self, $state) = @_;
	$self->{server}->setState($state);
}


######################
## Client Functions ##
######################

sub clientAlive {
	my $self = shift;
	return $self->proxyAlive();
}

sub proxyAlive {
	my $self = shift;
	return $self->{proxy} && $self->{proxy}->connected;
}

sub clientPeerHost {
	my $self = shift;
	return $self->{proxy}->peerhost if ($self->proxyAlive());
	return undef;
}

sub clientPeerPort {
	my $self = shift;
	return $self->{proxy}->peerport if ($self->proxyAlive());
	return undef;
}

sub clientSend {
	my $self = shift;
	my $msg = shift;
	my $dontMod = shift;

	return unless ($self->proxyAlive());

	$msg = $self->modifyPacketIn($msg) unless ($dontMod);
	if ($config{debugPacket_ro_received}) {
		debug "Modified packet sent to client\n";
		visualDump($msg, 'clientSend');
	}

	# queue message instead of sending directly
	$clientBuffer .= $msg;
}

sub clientFlush {
	my $self = shift;

	return unless (length($clientBuffer));

	$self->{proxy}->send($clientBuffer);
	debug "Client network buffer flushed out\n";
	$clientBuffer = '';
}

sub clientRecv {
	my ($self, $msg) = @_;

	return undef unless ($self->proxyAlive() && dataWaiting(\$self->{proxy}));

	$self->{proxy}->recv($msg, 1024 * 32);
	if (length($msg) == 0) {
		# Connection from client closed
		close($self->{proxy});
		$self->{proxy} = undef;
		message "XKore Proxy: RO Client disconnected.\n", "connection";
		return undef;
	}

	my $packet_id = unpack("v",$msg);
	my $switch = sprintf("%04X", $packet_id);

	if ($masterServer->{serverType} eq 'vRO') {
		# if ($switch eq '0B04') {
		if ($messageSender->{packet_list}->{$switch}->[0] eq 'master_login') {
			$messageSender->sendMasterLogin($config{username}, $config{password},
				$masterServer->{master_version}, $masterServer->{version});
			return undef;
		}
	}

	if ($messageSender->{packet_list}->{$switch}->[0] eq 'sync') {
		return undef;
	}

	if($self->getState() eq Network::IN_GAME || $self->getState() eq Network::CONNECTED_TO_CHAR_SERVER) {
		$self->onClientData($msg);
		return undef;
	}

	return $msg;
}

sub onClientData {
	my ($self, $msg) = @_;
	my $additional_data;
	my $type;

	while (my $message = $incomingMessages->readNext(\$type)) {
		$msg .= $message;
	}
	$self->decryptMessageID(\$msg);

	$msg = $incomingMessages->slicePacket($msg, \$additional_data); # slice packet if needed

	$incomingMessages->add($msg, 1);

	$messageSender->sendToServer($_) for $messageSender->process(
		$incomingMessages, $clientPacketHandler
	);

	$incomingMessages->clear();

	if($additional_data) {
		$self->onClientData($additional_data);
	}
}

sub checkConnection {
	my $self = shift;

	# Check connection to the client
	$self->checkProxy();

	# Check server connection
	$self->checkServer();
}

sub startProxy {
	my $self = shift;

	if (!defined($self->{proxy_listens}->{$self->{proxyPort}})) {
		$self->{proxy_listens}->{$self->{proxyPort}} = new IO::Socket::IP(
			LocalAddr	=> $self->{proxyListenIp},
			LocalPort	=> $self->{proxyPort},
			Listen		=> 5,
			Proto		=> 'tcp',
			Sockopts => [
				[ SOL_SOCKET, SO_KEEPALIVE ],
			],
			ReuseAddr   => 1);
		die "Unable to start the X-Kore proxy (" . $self->{proxyListenIp} . ":" . $self->{proxyPort} . "): $@\n" .
			"Make sure no other servers are running on port " . $self->{proxyPort} . "." unless $self->{proxy_listens}->{$self->{proxyPort}};
		$self->{proxy_listens}->{$self->{proxyPort}}->blocking(0);
	}
	message TF("Waiting Ragnarok Client to connect on (%s:%s)\n", ($self->{proxyListenIp} eq '127.0.0.1' ? 'localhost' : $self->{proxyListenIp}), $self->{proxyPort}), "startup";
}

sub startDivert {
	my $self = shift;

	my $divertedPort = sprintf("%d:%d:%d", $self->{nextPort}, $self->{proxyPort}, $self->{altPort});
	my $divertedIdx = 0;
	foreach (@{$self->{divertedPorts}}) {
		if ($_ eq $divertedPort) {
			debug "Ports $divertedPort are already diverted\n", "connection";
			$self->{divertIndex} = ($divertedIdx + 1) % 3;
			return;
		}
		$divertedIdx++;
	}
	message "Diverting ports $divertedPort\n", "connection";
	if ($DivertPort->Call($self->{divertIndex}, $self->{nextPort},$self->{proxyPort}, $self->{altPort})) {
		die "Error: DivertPort failed: " . (Win32::FormatMessage(Win32::GetLastError())) 
	}
	$self->{divertedPorts}[$self->{divertIndex}] = $divertedPort;
	$self->{divertIndex} = ($self->{divertIndex} + 1) % 3;
	debug "Next server to connect ($self->{nextIp}:$self->{altPort})\n", "connection";
}

sub setUpNextServer {
	my $self = shift;

	# Setup the next server to connect.
	if (!$self->{nextIp}) {
		$self->{nextIp} = $masterServer->{ip};
	}
	if (!$self->{nextPort}) {
		$self->{nextPort} = $masterServer->{port};
	}
	$self->{proxyPort} = $self->{nextPort} + 1001;
	$self->{altPort} = $self->{nextPort} + 1002;
	$self->startDivert();
}

sub connectToNextServer {
	my $self = shift;

	$self->setUpNextServer();
	$self->startProxy();
	$self->serverDisconnect(1);
}

sub checkProxy {
	my $self = shift;

	if ($self->proxyAlive()) {
		if (defined($self->{packetPending}) && length($self->{packetPending})) {
			checkPacketReplay();
		}
		return;
	}

	# Client disconnected... (or never existed)
	if ($self->serverAlive()) {
		message T("Client disconnected\n"), "connection";
		$self->setState(Network::NOT_CONNECTED) if ($self->getState() == Network::IN_GAME);
		$self->serverDisconnect();
	}

	if (defined($self->{proxy})) {
		close($self->{proxy});
		$self->{proxy} = undef;
	}
	$self->{waitClientDC} = undef;
	if (defined($self->{packetPending}) && length($self->{packetPending}) > 0) {
		debug "Removing pending packet from queue\n", "connection";
		$self->{packetPending} = '';
	}

	# setup master server if necessary
	getMainServer();

	$self->setUpNextServer();

	# (Re)start listening...
	if (!defined($self->{proxy_listens}->{$self->{proxyPort}})) {
		$self->{proxyListenIp} = ($masterServer->{gameGuard} eq 0 ) ? $config{XKore_listenIp} : '0.0.0.0';
		$self->startProxy();
	}

	if (defined($self->{proxy_listens}->{$self->{proxyPort}})) {
		# Listening for a client
		if (dataWaiting($self->{proxy_listens}->{$self->{proxyPort}})) {
			# Client is connecting...
			$self->{proxy} = $self->{proxy_listens}->{$self->{proxyPort}}->accept();

			# Tell 'em about the new client
			my $host = $self->clientPeerHost();
			my $port = $self->clientPeerPort();
			message "XKore Proxy: RO Client connected ($host:$port).\n", "connection";

			# Stop listening and clear errors.
			$self->{gotError} = 0;
			$self->{waitClientDC} = undef;
		}
		#return;
	}
}

sub checkServer {
	my $self = shift;

	# Do nothing until the client has (re)connected to us
	return if (!$self->proxyAlive() || $self->{waitClientDC});

	# Connect to the next server for proxying the packets
	if (!$self->serverAlive()) {

		$self->setUpNextServer();

		message TF("Proxying to [%s]\n", $config{master}), "connection" unless ($self->{gotError});
		eval {
			if (!$clientPacketHandler) {
				$clientPacketHandler = Network::ClientReceive->new;
			}
			if (!$packetParser) {
				$packetParser = Network::Receive->create($self, $masterServer->{serverType});
			}
			if (!$messageSender) {
				$messageSender = Network::Send->create($self, $masterServer->{serverType});
			}
		};
		if (my $e = caught('Exception::Class::Base')) {
			$interface->errorDialog($e->message());
			$quit = 1;
			return;
		}

		$self->serverConnect($self->{nextIp}, $self->{altPort}) unless ($self->{gotError});
		if (!$self->serverAlive()) {
			$self->{charServerIp} = undef;
			$self->{charServerPort} = undef;
			close($self->{proxy});
			$self->{proxy} = undef;
			error T("Invalid server specified or server does not exist...\n"), "connection" if (!$self->{gotError});
			$self->{gotError} = 1;
		}

		# clean Next Server uppon connection
		$self->{nextIp} = undef;
		$self->{nextPort} = undef;
	}
}

##
# $Network_XKoreProxy->checkPacketReplay()
#
# Setup a timer to repeat the received logon/server change packet to the client
# in case it didn't responded in an appropriate time.
#
# This is an internal function.
sub checkPacketReplay {
	my $self = shift;

	#message "Pending packet check\n";

	if ($self->{replayTimeout}{time} && timeOut($self->{replayTimeout})) {
		if ($self->{packetReplayTrial} < 3) {
			warning TF("Client did not respond in time.\n" .
				"Trying to replay the packet for %s of 3 times\n", $self->{packetReplayTrial}++);
			$self->clientSend($self->{packetPending});
			$self->{replayTimeout}{time} = time;
			$self->{replayTimeout}{timeout} = 2.5;
		} else {
			error T("Client did not respond. Forcing disconnection\n");
			close($self->{proxy});
			$self->{proxy} = undef;
			return;
		}

	} elsif (!$self->{replayTimeout}{time}) {
		$self->{replayTimeout}{time} = time;
		$self->{replayTimeout}{timeout} = 2.5;
	}
}

sub modifyPacketIn {
	my ($self, $msg) = @_;

	return undef if (length($msg) < 1);

	my $switch = uc(unpack("H2", substr($msg, 1, 1))) . uc(unpack("H2", substr($msg, 0, 1)));

	# packet replay check: reset status for every different packet received
	if ($self->{packetPending} && $self->{packetPending} ne $msg) {
		debug "Removing pending packet from queue\n", "connection";
		use bytes; no encoding 'utf8';
		delete $self->{replayTimeout};
		$self->{packetPending} = '';
		$self->{packetReplayTrial} = 0;
	} elsif ($self->{packetPending} && $self->{packetPending} eq $msg) {
		# avoid doubled 0259 message: could mess the character selection and hang up the client
		# if ($switch eq "0259") {
		# 	debug T("Logon-grant packet received twice! Avoiding bug in client.\n");
		# 	$self->{packetPending} = undef;
		# 	return undef;
		# }
	}

	# server list
	# if ($switch eq "0069" || $switch eq "0AC4" || $switch eq "0AC9" || $switch eq "0B07") {
	if ($packetParser->{packet_list}->{$switch}->[0] eq 'account_server_info') {
		use bytes; no encoding 'utf8';

		# queue the packet as requiring client's response in time
		$self->{packetPending} = $msg;

		if ($masterServer->{gameGuard} eq 0) {
			debug "Modifying Account Info packet...\n", "connection";
		}

		# Show list of character servers.
		my $serverCount = 0;
		my @serverList;
		my ($ip, $port, $name);
		if($config{server} =~/\d+/) {
			my %charServer;
			foreach $charServer (@servers) {
				if($serverCount == $config{server}) {
					$ip = $self->{publicIP} || $self->{proxy}->sockhost;
					$port = $self->{proxy}->sockport;
					$name = $charServer->{name}.' (PROXIED)';
					$self->{nextIp} = $charServer->{'ip'};
					$self->{nextPort} = $charServer->{'port'};
					$self->{charServerIp} = $charServer->{'ip'};
					$self->{charServerPort} = $charServer->{'port'};
				} else {
					$ip = $charServer->{ip};
					$port = $charServer->{port};
					$name = $charServer->{name};
				}

				push @serverList, {
					ip => $ip,
					port => $port,
					name => $name,
					users => $charServer->{users},
					display => $charServer->{display}, # don't show number of players
					state => $charServer->{state},
					property => $charServer->{property},
					unknown => 0,
					ip_port => $ip.':'.$port,
				};

				$serverCount++;
			}
		} else {
			$ip = $self->{publicIP} || $self->{proxy}->sockhost;
			$name = $servers[0]{'name'}.' (PROXIED)';
			$port = $self->{proxy}->sockport;
			$self->{nextIp} = $servers[0]{'ip'};
			$self->{nextPort} = $servers[0]{'port'};
			$self->{charServerIp} = $servers[0]{'ip'};
			$self->{charServerPort} = $servers[0]{'port'};
			push @serverList, {
					ip => $ip,
					port => $port,
					name => $name,
					users => $charServer->{users},
					display => $charServer->{display}, # don't show number of players
					state => $charServer->{state},
					property => $charServer->{property},
					unknown => 0,
					ip_port => $ip.':'.$port,
				};
		}

		if ($masterServer->{gameGuard} eq 0) {
			$msg = $packetParser->reconstruct({
				len => 100,
				switch => $switch,
				sessionID => $sessionID,
				accountID => $accountID,
				sessionID2 => $sessionID2,
				accountSex => $accountSex,
				lastLoginIP => "",
				lastLoginTime => "",
				unknown => "",
				servers => \@serverList,
			});
			substr($msg, 2, 2) = pack('v', length($msg));
		}

		message T("Closing connection to Account Server\n"), 'connection' if (!$self->{packetReplayTrial});
		$self->connectToNextServer();
		$self->clientSend($msg, 1);
		$self->clientFlush();
		return undef;

	# } elsif ($switch eq "0071" || $switch eq "0AC5") { # login in map-server
	} elsif ($packetParser->{packet_list}->{$switch}->[0] eq 'received_character_ID_and_Map') { # login in map-server
		my ($mapInfo, $server_info);

		# queue the packet as requiring client's response in time
		$self->{packetPending} = $msg;

		# Proxy the Logon to Map server
		debug "Modifying Map Logon packet...\n", "connection";

		if ($switch eq '0AC5') { # cRO 2017
			$server_info = {
				types => 'a4 Z16 a4 v a128',
				keys => [qw(charID mapName mapIP mapPort mapUrl)],
			};

		} else {
			$server_info = {
				types => 'a4 Z16 a4 v',
				keys => [qw(charID mapName mapIP mapPort)],
			};
		}

		my $ip = $self->{publicIP} || $self->{proxy}->sockhost;
		my $port = $self->{proxy}->sockport;

		@{$mapInfo}{@{$server_info->{keys}}} = unpack($server_info->{types}, substr($msg, 2));

		if (exists $mapInfo->{mapUrl} && $mapInfo->{'mapUrl'} =~ /.*\:\d+/) { # in cRO we have server.alias.com:port
			@{$mapInfo}{@{[qw(mapIP port)]}} = split (/\:/, $mapInfo->{'mapUrl'});
			$mapInfo->{mapIP} =~ s/^\s+|\s+$//g;
			$mapInfo->{port} =~ tr/0-9//cd;
		} else {
			$mapInfo->{mapIP} = inet_ntoa($mapInfo->{mapIP});
		}

		if($masterServer->{'private'}) {
			$mapInfo->{mapIP} = $masterServer->{ip};
		}

		if ($masterServer->{gameGuard} eq 0) {
			$msg = $packetParser->reconstruct({
				switch => $switch,
				charID => $mapInfo->{'charID'},
				mapName => $mapInfo->{'mapName'},
				mapIP => inet_aton($ip),
				mapPort => $port,
				mapUrl => $ip.':'.$port,
			});
		}

		$self->{nextIp} = $mapInfo->{'mapIP'};
		$self->{nextPort} = $mapInfo->{'mapPort'};

		# reset key when change map-server
		if ($currentClientKey && $messageSender->{encryption}->{crypt_key}) {
			$currentClientKey = $messageSender->{encryption}->{crypt_key_1};
			$messageSender->{encryption}->{crypt_key} = $messageSender->{encryption}->{crypt_key_1};
		}

		message T("Closing connection to Character Server\n"), 'connection' if (!$self->{packetReplayTrial});
		$self->connectToNextServer();
		$self->clientSend($msg, 1);
		$self->clientFlush();
		return undef;

	# } elsif($switch eq "0092" || $switch eq "0AC7" || $switch eq "0A4C") { # In Game Map-server changed
	} elsif($packetParser->{packet_list}->{$switch}->[0] eq 'map_changed') { # In Game Map-server changed
		my ($mapInfo, $server_info);

		if ($switch eq '0AC7') { # cRO 2017
			$server_info = {
				types => 'Z16 v2 a4 v a128',
				keys => [qw(map x y IP port url)],
			};

		} else {
			$server_info = {
				types => 'Z16 v2 a4 v',
				keys => [qw(map x y IP port)],
			};
		}

		my $ip = $self->{publicIP} || $self->{proxy}->sockhost;
		my $port = $self->{proxy}->sockport;

		@{$mapInfo}{@{$server_info->{keys}}} = unpack($server_info->{types}, substr($msg, 2));

		if (exists $mapInfo->{url} && $mapInfo->{'url'} =~ /.*\:\d+/) { # in cRO we have server.alias.com:port
			@{$mapInfo}{@{[qw(ip port)]}} = split (/\:/, $mapInfo->{'url'});
			$mapInfo->{ip} =~ s/^\s+|\s+$//g;
			$mapInfo->{port} =~ tr/0-9//cd;
		} else {
			$mapInfo->{ip} = inet_ntoa($mapInfo->{'IP'});
		}

		if($masterServer->{'private'}) {
			$mapInfo->{ip} = $masterServer->{ip};
		}

		if ($masterServer->{gameGuard} eq 0) {
			$msg = $packetParser->reconstruct({
				switch => $switch,
				map => $mapInfo->{'map'},
				x => $mapInfo->{'x'},
				y => $mapInfo->{'y'},
				IP => inet_aton($ip),
				port => $port,
				url => $ip.':'.$port,
			});
		}

		$self->{nextIp} = $mapInfo->{ip};
		$self->{nextPort} = $mapInfo->{'port'};

		# reset key when change map-server
		if ($currentClientKey && $messageSender->{encryption}->{crypt_key}) {
			$currentClientKey = $messageSender->{encryption}->{crypt_key_1};
			$messageSender->{encryption}->{crypt_key} = $messageSender->{encryption}->{crypt_key_1};
		}

		$self->connectToNextServer();
		$self->clientSend($msg, 1);
		$self->clientFlush();
		return undef;

	# } elsif ($switch eq "006A" || $switch eq "006C" || $switch eq "0081") {
	} elsif ($packetParser->{packet_list}->{$switch}->[0] eq 'login_error_game_login_server') {
		# An error occurred. Restart proxying
		$self->{gotError} = 1;
		$self->{nextIp} = undef;
		$self->{nextPort} = undef;
		$self->{charServerIp} = undef;
		$self->{charServerPort} = undef;
		$self->serverDisconnect(1);

	# } elsif ($switch eq "00B3") {
	} elsif ($packetParser->{packet_list}->{$switch}->[0] eq 'switch_character') {
		$self->{nextIp} = $self->{charServerIp};
		$self->{nextPort} = $self->{charServerPort};
		$self->connectToNextServer();
		$self->clientSend($msg, 1);
		$self->clientFlush();
		return undef;

	# } elsif ($switch eq "0259") {
	} elsif ($packetParser->{packet_list}->{$switch}->[0] eq 'login_pin_code_request' && $masterServer->{serverType} eq 'vRO') {
		my $seed = unpack("V", substr($msg,  2, 4));
		my $accountID = unpack("a4", substr($msg,  6, 4));
		my $flag = unpack("v", substr($msg,  10, 2));
		
		if ($flag == 1) {
			$self->clientSend($msg, 1);
			$self->clientFlush();
			$messageSender->sendLoginPinCode($seed, 0);
			return undef;
		}
	}

	return $msg;
}

sub getMainServer {
	if ($config{'master'} eq "" || $config{'master'} =~ /^\d+$/ || !exists $masterServers{$config{'master'}}) {
		my @servers = sort { lc($a) cmp lc($b) } keys(%masterServers);
		my $choice = $interface->showMenu(
			T("Please choose a master server to connect to."),
			\@servers,
			title => T("Master servers"));
		if ($choice == -1) {
			exit;
		} else {
			configModify('master', $servers[$choice], 1);
		}
	}
}

sub decryptMessageID {
	my ($self, $r_message) = @_;

	if(!$messageSender->{encryption}->{crypt_key} && $messageSender->{encryption}->{crypt_key_3}) {
		$currentClientKey = $messageSender->{encryption}->{crypt_key_1};
	} elsif(!$currentClientKey) {
		return;
	}

	my $messageID = unpack("v", $$r_message);

	# Saving Last Informations for Debug Log
	my $oldMID = $messageID;
	my $oldKey = ($currentClientKey >> 16) & 0x7FFF;

	# Calculating the Encryption Key
	$currentClientKey = ($currentClientKey * $messageSender->{encryption}->{crypt_key_3} + $messageSender->{encryption}->{crypt_key_2}) & 0xFFFFFFFF;

	# Xoring the Message ID
	$messageID = ($messageID ^ (($currentClientKey >> 16) & 0x7FFF)) & 0xFFFF;
	$$r_message = pack("v", $messageID) . substr($$r_message, 2);

	# Debug Log
	debug (sprintf("Decrypted MID : [%04X]->[%04X] / KEY : [0x%04X]->[0x%04X]\n", $oldMID, $messageID, $oldKey, ($currentClientKey >> 16) & 0x7FFF), "sendPacket", 0), "connection" if $config{debugPacket_sent};
}

return 1;
