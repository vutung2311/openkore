#########################################################################
#  OpenKore - Network subsystem
#  This module contains functions for sending messages to the server.
#
#  This software is open source, licensed under the GNU General Public
#  License, version 2.
#  Basically, this means that you're allowed to modify and distribute
#  this software. However, if you distribute modified versions, you MUST
#  also distribute the source code.
#  See http://www.gnu.org/licenses/gpl.html for the full license.
#########################################################################
# vRO (Vietnam)
# Servertype overview: https://openkore.com/wiki/ServerType
package Network::Send::vRO;

use strict;
use Digest::MD5 qw(md5_hex);
# Make sure LWP::UserAgent uses the right kind of socket
use Net::SSL;
use LWP::UserAgent;
use JSON;
use URI;
use Translation qw(T TF);

use Network::Send::ServerType0;
use base qw(Network::Send::ServerType0);
use Globals qw(%config $masterServer);
use Log qw(debug message warning error);
use Misc qw(configModify);
use Plugins;

# by ya4epT v1.0 (2021-07-10)
# this plugin allows you to get tokens for authorization on vRO
# https://ro.vtcgame.vn/
# https://apisdk.vtcgame.vn/sdk/login?username=<USERNAME>&password=<MD5-PASSWORD>&client_id=<MD5-CLIENT_ID>&client_secret=<MD5-CLIENT_SECRET>&grant_type=password&authen_type=0&device_type=1

my $agent = LWP::UserAgent->new(
	agent => 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36',
	ssl_opts => { 
		verify_hostname => 0,
		SSL_cipher_list => '',
	},
);

sub getToken {
	my ($accessToken, $billingAccessToken);

	my $username = $config{username};
	my $md5Password = md5_hex($config{password});
	my $md5ClientID = '2aa32a67b771fcab4fd501273ef8b744';
	my $md5ClientSecret = '9ecf8255d241f5e702714734e3a93afb';

	my $url = URI->new("https://apisdk.vtcgame.vn/sdk/login");
	$url->query_form(
		username => $username,
		password => $md5Password,
		client_id => $md5ClientID,
		client_secret => $md5ClientSecret,
		grant_type => 'password',
		authen_type => '0',
		device_type => '1',
	);
	debug "[vRO_auth] URL: $url\n\n";

	message TF("[vRO_auth] Getting access token\n");

	my $res = $agent->get($url);
	die "Failed: ", $res->status_line(), "\n" unless $res->is_success();

	my $content = $res->decoded_content();
	die "[vRO_auth] Error: the request returned an empty result\n" if (length($content) eq 0);
	debug "[vRO_auth] Content: $content\n";

	my $jsonContent = from_json($content);
	die "[vRO_auth] Error: $content (Incorrect account or password)\n" if ($jsonContent->{error} eq "-349");
	die "[vRO_auth] Error: $content (Unknown error)\n" unless ($jsonContent->{error} eq "200");

	$accessToken = $jsonContent->{info}->{accessToken};
	$billingAccessToken = $jsonContent->{info}->{billingAccessToken};
	die "Empty token in response" unless ($accessToken and $billingAccessToken);
	configModify ('accessToken', $accessToken, 1);
	configModify ('billingAccessToken', $billingAccessToken, 1);

	debug "[vRO_auth] accessToken: $accessToken\n".
		"[vRO_auth] billingAccessToken: $billingAccessToken\n";
}

sub new {
	my ($class) = @_;
	my $self = $class->SUPER::new(@_);

	my %packets = (
		'0436' => ['map_login', 'a4 a4 a4 V2 C', [qw(accountID charID sessionID unknown tick sex)]],#23
		'0B04' => ['master_login', 'V Z30 Z52 Z100 v', [qw(version username accessToken billingAccessToken master_version)]],# 190
	);

	$self->{packet_list}{$_} = $packets{$_} for keys %packets;

	my %handlers = qw(
		actor_action 0437
		actor_info_request 0368
		actor_look_at 0361
		actor_name_request 0369
		char_create 0A39
		character_move 035F
		item_drop 0363
		item_take 0362
		map_login 0436
		master_login 0B04
		party_setting 07D7
		pet_capture 019F
		rodex_open_mailbox 09E8
		rodex_refresh_maillist 09EF
		send_equip 0998
		skill_use 0438
		skill_use_location 0366
		storage_item_add 0364
		storage_item_remove 0365
		sync 0360
	);
	$self->{packet_lut}{$_} = $handlers{$_} for keys %handlers;

	$self->{send_buy_bulk_pack} = "v V";
	$self->{char_create_version} = 1;
	$self->{send_sell_buy_complete} = 1;
	return $self;
}

sub sendMasterLogin {
	my ($self, $username, $password, $master_version, $version) = @_;
	my $msg;

	$self->getToken();
	my $accessToken = $config{accessToken};
	my $billingAccessToken = $config{billingAccessToken};

	$msg = $self->reconstruct({
		switch => 'master_login',
		version => $version || $self->version,
		master_version => $master_version,
		username => $username,
		accessToken => $accessToken,
		billingAccessToken => $billingAccessToken,
	});

	$self->sendToServer($msg);
	debug "Sent sendMasterLogin\n", "sendPacket", 2;
}

1;
