/* Utility functions rewritten in C for speed */
#include <stdio.h>
#include <string.h>
#include <string>
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"


using namespace std;


MODULE = FastUtils	PACKAGE = Utils
PROTOTYPES: ENABLE

char *
xpmmake(width, height, field_data)
	int width
	int height
	char *field_data
CODE:
	// Create an XPM from raw field data.
	// Written in C++ for speed.
	string data;
	int y, x;
	char tmp[10], *buf, *line;

	data.reserve (width * height + 1024);
	data.append (
		"/* XPM */\n"
		"static char * my_xpm[] = {\n"
		"\"");
	snprintf (tmp, sizeof (tmp), "%d %d", width, height);
	data.append (tmp);
	data.append (" 8 1\",\n"
		"\"A\tc #F4F4F4\",\n"
		"\"B\tc #505050\",\n"
		"\"C\tc #6060B0\",\n"
		"\"D\tc #8080B0\",\n"
		"\"E\tc #7070B0\",\n"
		"\"F\tc #B0B0B0\",\n"
		"\"G\tc #808080\",\n"
		"\"H\tc #600000\",\n");

	line = (char *) malloc (width);
	for (y = height - 1; y >= 0; y--) {
		for (x = 0; x < width; x++) {
			switch (field_data[y * width + x]) {
			case '\0':
				line[x] = 'A';
				break;
			case '\1':
				line[x] = 'B';
				break;
			case '\2':
				line[x] = 'C';
				break;
			case '\3':
				line[x] = 'D';
				break;
			case '\4':
				line[x] = 'E';
				break;
			case '\5':
				line[x] = 'F';
				break;
			case '\6':
				line[x] = 'G';
				break;
			default:
				line[x] = 'H';
				break;
			}
		}
		data.append ("\"");
		data.append (line, width);
		data.append ("\",\n");
	}
	free (line);
	data.append ("};\n");

	// I hope sizeof(char) == 1...
	New (0, buf, data.size () + 1, char);
	Copy (data.c_str (), buf, data.size (), char);
	buf[data.size ()] = '\0';
	RETVAL = buf;
OUTPUT:
	RETVAL


SV *
makeDistMap(rawMap, width, height)
	SV *rawMap
	int width
	int height
INIT:
	STRLEN len;
	int i, x, y;
	int dist_current, val;
	unsigned char *c_rawMap, *data;
	bool done;
CODE:
	if (!SvOK (rawMap))
		XSRETURN_UNDEF;

	c_rawMap = (unsigned char *) SvPV (rawMap, len);
	if ((int) len != width * height)
		XSRETURN_UNDEF;

	/* Simplify the raw map data. Each byte in the raw map data
	   represents a block on the field, but only some bytes are
	   interesting to pathfinding. */
	New (0, data, len, unsigned char);
	Copy (c_rawMap, data, len, unsigned char);
	int info;
	int walkable;
	for (i = 0; i < (int) len; i++) {
		// first bit is 'walkable' info
		info = data[i];
		walkable = (info & 1) ? 1 : 0;
		if (walkable == 1) {
			data[i] = 255;
		} else {
			data[i] = 0;
		}
	}

	done = false;
	while (!done) {
		done = true;

		// 'push' wall distance right and up
		for (y = 0; y < height; y++) {
			for (x = 0; x < width; x++) {
				i = y * width + x; // i: cell to examine
				
				if (data[i] > 0 && (x == 0 || y == 0 || x == width - 1 || y == height - 1)) {
					data[i] = 1;
				}
				
				dist_current = data[i]; // dist_current: initial dist of i from walkable/nonwalkable check above
				
				if (x != width - 1) {
					int east_cell = y * width + x + 1; // ir: cell to the right
					int dist_east_cell = (int) data[east_cell]; // dist_east_cell: initial dist of ir from walkable/nonwalkable check above
					int delta_dist = dist_current - dist_east_cell; // delta_dist: 
					if (delta_dist > 1) { // dist_current > dist_east_cell: real dist_current is dist_east_cell + 1
						val = dist_east_cell + 1;
						if (val > 255) {
							val = 255;
						}
						data[i] = val;
						done = false;
					} else if (delta_dist < -1) { // dist_current < dist_east_cell: real dist_east_cell is dist_current + 1
						val = dist_current + 1;
						if (val > 255) {
							val = 255;
						}
						data[east_cell] = val;
						done = false;
					}
				}

				if (y != height - 1) {
					int north_cell = (y + 1) * width + x;
					int dist_north_cell = (int) data[north_cell];
					int delta_dist = dist_current - dist_north_cell;
					if (delta_dist > 1) {
						int val = dist_north_cell + 1;
						if (val > 255) {
							val = 255;
						}
						data[i] = (char) val;
						done = false;
					} else if (delta_dist < -1) {
						int val = dist_current + 1;
						if (val > 255) {
							val = 255;
						}
						data[north_cell] = (char) val;
						done = true;
					}
				}
			}
		}

		// 'push' wall distance left and down
		for (y = height - 1; y >= 0; y--) {
			for (x = width - 1; x >= 0 ; x--) {
				i = y * width + x;
				dist_current = data[i];
				
				if (x != 0) {
					int west_cell = y * width + x - 1;
					int dist_west_cell = data[west_cell];
					int delta_dist = dist_current - dist_west_cell;
					if (delta_dist > 1) {
						val = dist_west_cell + 1;
						if (val > 255) {
							val = 255;
						}
						data[i] = val;
						done = false;
					} else if (delta_dist < -1) {
						val = dist_current + 1;
						if (val > 255) {
							val = 255;
						}
						data[west_cell] = val;
						done = false;
					}
				}
				
				if (y != 0) {
					int south_cell = (y - 1) * width + x;
					int dist_south_cell = data[south_cell];
					int delta_dist = dist_current - dist_south_cell;
					if (delta_dist > 1) {
						val = dist_south_cell + 1;
						if (val > 255) {
							val = 255;
						}
						data[i] = val;
						done = false;
					} else if (delta_dist < -1) {
						val = dist_current + 1;
						if (val > 255) {
							val = 255;
						}
						data[south_cell] = val;
						done = false;
					}
				}
			}
		}
	}

	RETVAL = newSVpv ((const char *) data, len);
OUTPUT:
	RETVAL


SV *
makeWeightMap(distMap, width, height)
	SV *distMap
	int width
	int height
INIT:
	STRLEN len;
	int i, x, y;
	int dist;
	char *c_weightMap, *data;
CODE:
	if (!SvOK (distMap))
		XSRETURN_UNDEF;

	c_weightMap = (char *) SvPV (distMap, len);
	if ((int) len != width * height)
		XSRETURN_UNDEF;

	/* Simplify the raw map data. Each byte in the raw map data
	   represents a block on the field, but only some bytes are
	   interesting to pathfinding. */
	New (0, data, len, char);
	Copy (c_weightMap, data, len, char);
	
	int distance_to_weight[6] = { -1, 60, 50, 20, 10, 0 };
	int max_distance = 5;

	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			i = y * width + x; // i: cell to examine
			dist = data[i]; // dist: dist of i from wall
			
			if (dist > max_distance) {
				dist = max_distance;
			}
			data[i] = distance_to_weight[dist];
		}
	}

	RETVAL = newSVpv ((const char *) data, len);
OUTPUT:
	RETVAL
