.PHONY: all clean dist bindist test

# NOTE TO DEVELOPERS:
# We use the SCons build system ( https://www.scons.org/ ). This makefile is just a wrapper around it.
#
# The real build system definitions are in the file SConstruct. If you need
# to change anything, edit SConstruct (or SConscript in the subfolders).
#
# If you experience any build problems, read this web page:
# https://openkore.com/wiki/How_to_run_OpenKore

PYTHON = python3
ifeq ($(OS),Windows_NT)
PYTHON = python
endif

all:
	@$(PYTHON) src/build/scons/scons.py || printf "\e[1;31mCompilation failed. Please read https://openkore.com/wiki/How_to_run_OpenKore for help.\e[0m\n"

doc:
	cd src/doc/ && ./createdoc.pl

test:
	cd src/test/ && perl unittests.pl

clean:
	@$(PYTHON) src/build/scons/scons.py -c

dist:
	bash makedist.sh

bindist:
	bash makedist.sh --bin
